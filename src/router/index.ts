import { createRouter, createWebHistory } from 'vue-router'

import admin from './admin'
import client from './client'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/pages/client/home.vue'),
      meta: {
        layout: 'Client'
      },
      children: [...client]
    },
    {
      path: '/admin/login',
      name: 'admin-login',
      component: () => import('@/pages/auth/login.vue'),
      meta: {
        layout: 'Default'
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('@/pages/admin/index.vue'),
      meta: {
        layout: 'Admin'
      },
      children: [...admin]
    }
  ]
})

export default router
